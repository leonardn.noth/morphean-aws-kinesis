#!/bin/bash

thingName="webrtc_iot_morphean_kinesis_thing"
thingTypeName="webrtc_iot_morphean_kinesis_thing_type"
iotPolicyName="webrtc_iot_morphean_kinesis_policy"
kvsPolicyName="webrtc_morphean_kinesis_policy"
iotRoleName="webrtc_iot_morphean_kinesis_role"
iotRoleAlias="webrtc_iot_morphean_kinesis_role_alias"
iotCert="webrtc_iot_certifcate.pem"
iotPublicKey="webrtc_iot_public.key"
iotPrivateKey="webrtc_iot_private.key"

certificateFolder="certificates"

# Step 0: Create the certificate folder
mkdir $certificateFolder

# Step 1: Create an IoT Thing Type
if aws --profile default  iot create-thing-type --thing-type-name $thingTypeName > $certificateFolder"/"iot-thing-type.json ; then
    echo "The creation of the $thingTypeName iot thing type was done successfully."
    echo ""
else :
    # Error will be printed in the terminal
fi

# Step 2: Create an IoT Thing with the proper type
if aws --profile default  iot create-thing --thing-name $thingName --thing-type-name $thingTypeName > $certificateFolder"/"iot-thing.json ; then
    echo "The creation of the $thingName iot thing was done successfully."
    echo ""
else :
    # Error will be printed in the terminal
fi


# Step 3: Create an IAM Policy that will be used by the IAM Role to allow IOT to assume the role.
echo '{
   "Version":"2012-10-17",
   "Statement":[
    {
     "Effect":"Allow",
     "Principal":{
        "Service":"credentials.iot.amazonaws.com"
     },
     "Action":"sts:AssumeRole"
    }
   ]
}' > $certificateFolder"/"iam-policy-document.json


# Step 4: Create an IAM Role using the IAM Policy created.
if aws --profile default  iam create-role --role-name $iotRoleName --assume-role-policy-document 'file://'$certificateFolder'/iam-policy-document.json' > $certificateFolder"/"iam-role.json ; then
    echo "The creation of the $iotRoleName IAM Role was done successfully."
    echo ""
else
    # Error will be printed in the terminal
    # It means that the role already exists. So we have to get the json to keep going.
    aws --profile default  iam get-role --role-name $iotRoleName > $certificateFolder"/"iam-role.json
fi


# Step 5: Create an IAM Persmissions and attach them to the IAM Role.
echo '{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "kinesisvideo:DescribeSignalingChannel",
                "kinesisvideo:CreateSignalingChannel",
                "kinesisvideo:DeleteSignalingChannel",
                "kinesisvideo:GetSignalingChannelEndpoint",
                "kinesisvideo:GetIceServerConfig",
                "kinesisvideo:ConnectAsMaster",
                "kinesisvideo:ConnectAsViewer"
            ],
            "Resource": "arn:aws:kinesisvideo:*:*:channel/${credentials-iot:ThingName}/*"
        }
    ]
}' > $certificateFolder"/"iam-permission-document.json


if aws --profile default iam put-role-policy --role-name $iotRoleName --policy-name $kvsPolicyName --policy-document 'file://'$certificateFolder"/"'iam-permission-document.json' ; then
    echo "The creation and attachement of the $kvsPolicyName IAM Policy to the $iotRoleName IAM Role was done successfully."
    echo ""
else :
    # Error will be printed in the terminal
fi
 

# Step 6: Create an IOT Role Alias for the IAM Role.
if aws --profile default  iot create-role-alias --role-alias $iotRoleAlias --role-arn $(jq --raw-output '.Role.Arn' $certificateFolder"/"iam-role.json) --credential-duration-seconds 3600 > $certificateFolder"/"iot-role-alias.json ; then
    echo "The creation of the $iotRoleAlias IAM Role Alias was done successfully."
else
    # Error will be printed in the terminal
    # It means that the role alias already exists. So we have to get the json to keep going.
    jq --raw-output '.roleAliasDescription' <<< $(aws iot describe-role-alias --role-alias $iotRoleAlias) > $certificateFolder"/"iot-role-alias.json

fi


# Step 7: Create an IOT policy.
cat > $certificateFolder"/"iot-policy-document.json <<EOF
{
   "Version":"2012-10-17",
   "Statement":[
      {
	 "Effect":"Allow",
	 "Action":[
	    "iot:Connect"
	 ],
	 "Resource":"$(jq --raw-output '.roleAliasArn' $certificateFolder"/"iot-role-alias.json)"
 },
      {
	 "Effect":"Allow",
	 "Action":[
	    "iot:AssumeRoleWithCertificate"
	 ],
	 "Resource":"$(jq --raw-output '.roleAliasArn' $certificateFolder"/"iot-role-alias.json)"
 }
   ]
}
EOF
 
if aws --profile default iot create-policy --policy-name $iotPolicyName --policy-document 'file://'$certificateFolder"/"'iot-policy-document.json' ; then
    echo "The creation of the $iotPolicyName IOT Policy was done successfully."
else :
    # Error will be printed in the terminal
fi

# Step 8: Create and Configure the X.509 Certificate
if aws --profile default  iot create-keys-and-certificate --set-as-active --certificate-pem-outfile $certificateFolder"/"$iotCert --public-key-outfile $certificateFolder"/"$iotPublicKey --private-key-outfile $certificateFolder"/"$iotPrivateKey > $certificateFolder"/"certificate ; then
    echo "The creation of the $iotCert X.509 Certificate was done successfully."
else :
    # Error will be printed in the terminal
fi

# Step 9: Attach the policy for IoT to this certificate.
if aws --profile default  iot attach-policy --policy-name $iotPolicyName --target $(jq --raw-output '.certificateArn' $certificateFolder"/"certificate) ; then
    echo "The attachement of the $iotPolicyName policy to the $iotCert X.509 Certificate was done successfully."
else :
    # Error will be printed in the terminal
fi

# Step 10: Attach the IoT thing to the certificate you just created
if aws --profile default  iot attach-thing-principal --thing-name $thingName --principal $(jq --raw-output '.certificateArn' $certificateFolder"/"certificate) ; then
    echo "The attachement of the $iotPolicyName policy to the $thingName thing was done successfully."
else :
    # Error will be printed in the terminal
fi

# Step 11: In order to authorize requests through the IoT credentials provider, you need the IoT credentials endpoint which is unique to your AWS account ID.
if aws --profile default  iot describe-endpoint --endpoint-type iot:CredentialProvider --output text > $certificateFolder"/"iot-credential-provider.txt ; then
    echo "Getting the IOT CredentialProvider was done successfully."
else :
    # Error will be printed in the terminal
fi

# Step 12: In addition to the X.509 cerficiate created above, you must also have a CA certificate to establish trust with the back-end service through TLS.
if curl --silent 'https://www.amazontrust.com/repository/SFSRootCAG2.pem' --output $certificateFolder"/"cacert.pem ; then
    echo "Getting the CA Certificate was done successfully."
else :
    # Error will be printed in the terminal
fi