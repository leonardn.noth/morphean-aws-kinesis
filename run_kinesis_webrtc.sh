export AWS_KVS_LOG_LEVEL=3

thingName="webrtc_iot_morphean_kinesis_thing"
thingTypeName="webrtc_iot_morphean_kinesis_thing_type"
iotPolicyName="webrtc_iot_morphean_kinesis_policy"
kvsPolicyName="webrtc_morphean_kinesis_policy"
iotRoleName="webrtc_iot_morphean_kinesis_role"
iotRoleAlias="webrtc_iot_morphean_kinesis_role_alias"
iotCert="webrtc_iot_certifcate.pem"
iotPublicKey="webrtc_iot_public.key"
iotPrivateKey="webrtc_iot_private.key"
certificateFolder="certificates"

export AWS_IOT_CORE_CREDENTIAL_ENDPOINT=$(cat $certificateFolder"/"iot-credential-provider.txt)
export AWS_IOT_CORE_CERT=$(pwd)"/"$certificateFolder"/"$iotCert
export AWS_IOT_CORE_PRIVATE_KEY=$(pwd)"/"$certificateFolder"/"$iotPrivateKey
export AWS_IOT_CORE_ROLE_ALIAS=$iotRoleAlias
export AWS_IOT_CORE_THING_NAME=$thingName


$(pwd)"/"aws_c_client/build/samples/kvsWebrtcClientMasterGstSample $AWS_IOT_CORE_THING_NAME
